import csv
import json
import boto3
import http.client

auth_id = "auth-id"
auth_token = "auth-token"

# Function to make an HTTP GET request
def make_http_request(url):
    conn = http.client.HTTPSConnection(url)
    conn.request('GET', '/')
    response = conn.getresponse()
    return response

def lambda_handler(event, context):
    # Extract fields from a CSV file in an S3 bucket
    s3_bucket = 'your-s3-bucket'  # Replace with your S3 bucket name
    s3_key = 'your-folder/your-file.csv'  # Replace with the specific S3 file path
    params_to_extract = ['address1', 'address2', 'locality', 'administrative_area', 'postal_code', 'country'] # edit fields

    try:
        # Download the CSV file from S3
        s3 = boto3.client('s3')
        response = s3.get_object(Bucket=s3_bucket, Key=s3_key)
        csv_data = response['Body'].read().decode('utf-8').splitlines()

        # Parse the CSV data
        csv_reader = csv.DictReader(csv_data)
        extracted_data = []

        for row in csv_reader:
            extracted_row = {key: row[key] for key in params_to_extract if key in row}
            extracted_data.append(extracted_row)

        print('Extracted Data:')
        print(json.dumps(extracted_data, indent=4))

        # Perform an HTTP GET request with the extracted data
        base_url = 'international-street.api.smarty.com&auth-id=' + auth_id + '&auth-token=' + auth_token
        query_string = urllib.parse.urlencode(extracted_data[0], doseq=True)
        path = f'/verify?{query_string}'
        response = make_http_request(base_url + path)

        if response.status == 200:
            response_data = response.read().decode('utf-8')
            print('Response Content:')
            print(response_data)
            return json.loads(response_data)
        else:
            print(f'Request failed with status code: {response.status}')
            return {
                'statusCode': response.status,
                'body': 'Request failed'
            }

    except Exception as error:
        print('An error occurred:', error)
        return {
            'statusCode': 500,
            'body': 'Internal Server Error'
        }
